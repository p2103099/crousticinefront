#Stage 1: Install dependencies
FROM node:18 AS build


RUN apt-get update && apt-get install -y wget gnupg2 ca-certificates \
    --no-install-recommends && \
    wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list && \
    apt-get update && \
    apt-get install -y google-chrome-stable && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean



ENV CHROME_BIN /usr/bin/google-chrome


WORKDIR /app


COPY package*.json ./

RUN npm install -g @angular/cli
RUN npm install
COPY . .
RUN ng build

CMD ["ng", "serve", "--host", "0.0.0.0"]   

