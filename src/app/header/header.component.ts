import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  scrollTo(section: string) {
    if(section === 'home') {
      window.scrollTo({top: 0, behavior: 'smooth'});
    } else {
      const footer = document.getElementById('footer');
      footer?.scrollIntoView({behavior: 'smooth'});
    }
  }
}
