// header.component.spec.ts
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { HeaderComponent } from './header.component';
import { MovieService } from '../services/movie.service';
import { HomeComponent } from '../home/home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [HeaderComponent],
      providers: [MovieService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the logo', () => {
    const logo = fixture.debugElement.query(By.css('.logo'));
    expect(logo.nativeElement.src).toContain('assets/images/logo_crousti.png');
  });

  it('should have the title CROUSTICINE', () => {
    const title = fixture.debugElement.query(By.css('h1')).nativeElement;
    expect(title.textContent).toContain('CROUSTICINE');
  });

  it('should have navigation links with correct text', () => {
    const navLinks = fixture.debugElement.queryAll(By.css('.navigation a'));
    expect(navLinks.length).toEqual(3);
    expect(navLinks[0].nativeElement.textContent).toContain('Accueil 🎬');
    expect(navLinks[1].nativeElement.textContent).toContain('À propos 🤔');
    expect(navLinks[2].nativeElement.textContent).toContain('Contact 📞');
  });

});
