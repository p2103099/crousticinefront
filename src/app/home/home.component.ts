import {Component, OnInit} from '@angular/core';
import {Movie} from "../interfaces/movie";
import {lastValueFrom, Observable} from "rxjs";
import {MovieService} from "../services/movie.service";
import {Router} from "@angular/router";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{

  favoriteAddedMessage: string = '';

  movies: Movie[] = [];

  showPopup = false;

  selectedMovie?: Movie;

  researchParam: string = "";

  constructor(private movieService: MovieService,
              private userService: UserService) {
  }

  async ngOnInit(): Promise<void> {

    await this.movieService.getAllMovie().subscribe(
      (receivedMovies: any) => {
        receivedMovies = receivedMovies.movies;
        this.movies = receivedMovies;
      }
    );

  }


  addMovieToFavorites() {
    if (this.selectedMovie && this.selectedMovie._id !== undefined) {
      const movieIdAsString = this.selectedMovie._id.toString();
      this.userService.addFavorite(movieIdAsString).subscribe({
        next: (response) => {
          this.favoriteAddedMessage = `${this.selectedMovie?.title} a été ajouté aux favoris`;
          setTimeout(() => this.favoriteAddedMessage = '', 3000);
          console.log('Film ajouté aux favoris', response);
        },
        error: (error) => {
          console.error("Erreur lors de l'ajout du film aux favoris", error);
        }
      });
    } else {
      console.error('ID du film est undefined');
    }
  }
  


  async makeSearch() {
    this.movies = await lastValueFrom(this.movieService.search(this.researchParam));
  }

  handleImageError(event:any) {
    event.target.src="./assets/images/default.avif"
  }
}
