import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MovieService } from './movie.service';
import { Movie } from '../interfaces/movie';

describe('MovieService', () => {
  let service: MovieService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MovieService]
    });
    service = TestBed.inject(MovieService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('getAllMovie should return expected movies', () => {
    const expectedMovies: Movie[] = [
      { _id: 1, title: 'Test Movie', plot: 'Test Plot', year: 2020, genres: ['Test'], runtime: 120, directors: ['Test Director'], poster: 'test.jpg' }
    ];

    service.getAllMovie().subscribe(movies => {
      expect(movies).toEqual(expectedMovies);
    });

    const expectedUrl = 'http://localhost:3000/movies';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('GET');
    req.flush(expectedMovies);
  });

  
});
