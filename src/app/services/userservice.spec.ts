// userservice.spec.ts

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  let httpTestingController: HttpTestingController;

  const apiUrl = 'http://localhost:3000'; 

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    service = TestBed.inject(UserService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('addFavorite should make POST request', () => {
    const testMovieId = '123';
    service.addFavorite(testMovieId).subscribe(response => {
     
    });

    
    const req = httpTestingController.expectOne(`${apiUrl}/addFavMovies`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({ movieId: testMovieId });
    req.flush({});
  });


});
