import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Movie} from "../interfaces/movie";

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  private baseUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getAllMovie(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.baseUrl + "/movies");
  }

  search(searchParam: string): Observable<Movie[]> {
    let queryParams = new HttpParams();
    queryParams = queryParams.append("queryTitle", searchParam);
    return this.http.get<Movie[]>(this.baseUrl + "/searchMovie", { params: queryParams });
  }
}
