import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private apiUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  addFavorite(movieId: string) {
    const body = { movieId };
    return this.http.post(`${this.apiUrl}/addFavMovies`, body);
  }
}
