export interface Movie {
  _id: number,
  title: string,
  plot: string,
  year: number,
  genres: string[],
  runtime: number,
  directors: string[],
  poster: string
}
