export interface User {
  username: string,
  password: string,
  favMovies: string[]
}
