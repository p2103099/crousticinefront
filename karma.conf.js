

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // laisser Jasmine Spec Runner afficher les résultats des tests dans le navigateur
    },
    coverageReporter: {
      dir: require('path').join(__dirname, './coverage/mon-app'),
      reports: ['html', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['ChromeHeadless'], // Utilisez ChromeHeadlessCI ici
    singleRun: true,
    restartOnFileChange: true,
    customLaunchers: {
      ChromeHeadlessCI: {
        base: 'ChromeHeadless',
        flags: [
          '--no-sandbox',
          '--disable-gpu',
          '--disable-translate',
          '--disable-extensions',
          '--remote-debugging-port=9222',
          '--disable-dev-shm-usage' // Ajouter si vous rencontrez des problèmes de mémoire avec Chrome dans CI
        ]
      }
    }
  });
};
